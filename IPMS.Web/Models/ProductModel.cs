﻿using System.Collections.Generic;

namespace IPMS.Web.Models
{
    public class ProductModel
    {
        public int ProductListid { get; set; }
        public double tempPRate { get; set; }
        public bool IsProductActive { get; set; }
        public string CategoryName { get; set; }
        public string BrandName { get; set; }
        public string SizeName { get; set; }
        public string WeightName { get; set; }
    }

    public partial class ProductViewModel
    {
        public string statusCode { get; set; }
        public string success { get; set; }
        public List<ProductModel> data { get; set; }
    }
}
