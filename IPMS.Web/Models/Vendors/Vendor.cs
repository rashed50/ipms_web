﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IPMS.Web.Models.Vendors
{
    public class Vendor
    {
        [Required]
        public string VendorName { get; set; }
        [Required]
        public string ContactNumber { get; set; }
        public string Address { get; set; }
        public string VendorPhoto { get; set; }
        [Required]
        public DateTime OpeningDate { get; set; }
        [Required]
        public double Balance { get; set; }
        [Required]
        public int InitialBalance { get; set; }
        [Required]
        public int VendorAccountId { get; set; }
        [Required]
        public int EntryById { get; set; }
    }
    public partial class VendorViewModel
    {
        public string statusCode { get; set; }
        public string success { get; set; }
        public List<Vendor> data { get; set; }
    }

}
