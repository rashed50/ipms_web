﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IPMS.Web.Models
{
    public class EmployeeModel
    {
        public string EmployeeName { get; set; }
        public string ContactNumber { get; set; }
        public string FatherName { get; set; }
        public string EmpMobileNo { get; set; }
        public string NID { get; set; }
        public string Photo { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "YYYY-MM-dd HH:mm:ss.fff")]
        public DateTime JoinDate { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "YYYY-MM-dd HH:mm:ss.fff")]

        public DateTime ReviewDate { get; set; }
        public int CountryId { get; set; }
        public int DivisionId { get; set; }
        public int DistrictId { get; set; }
        public int UpazilaId { get; set; }
        public int FactoryId { get; set; }
        public int FactoryUnitId { get; set; }
        public int DesignationId { get; set; }
        public int EmployeeTypeId { get; set; }
        public int SalaryGradeId { get; set; }
        public int PresentSalary { get; set; }
        public int StartingSalary { get; set; }

        //public List<object> GetAllEmployee()
        //      {

        //      }
    }

    public partial class EmployeeViewModel
    {
        public string statusCode { get; set; }
        public string success { get; set; }
        public List<EmployeeModel> data { get; set; }
    }

    public partial class DivisionViewModel
    {
        public string statusCode { get; set; }
        public string success { get; set; }
        public List<Division> data { get; set; }
    }

    public partial class Division
    {
        public int DivisionId { get; set; }
        public string DivisionName { get; set; }
        public int CountryId { get; set; }
    }

    public partial class DistrictViewModel
    {
        public string statusCode { get; set; }
        public string success { get; set; }
        public List<District> data { get; set; }
    }

    public partial class District
    {
        public int DistrictId { get; set; }
        public string DistrictName { get; set; }
        public int DivisionId { get; set; }
    }

    public partial class Country
    {
        public int CountryId { get; set; }
        public string CountryName { get; set; }
    }

    public partial class CountryViewModel
    {
        public string statusCode { get; set; }
        public string success { get; set; }
        public List<Country> data { get; set; }
    }

    public partial class Upazila
    {
        public int UpazilaId { get; set; }
        public string UpazilaName { get; set; }
        public int DistrictId { get; set; }
    }

    public partial class UpazilaViewModel
    {
        public string statusCode { get; set; }
        public string success { get; set; }
        public List<Upazila> data { get; set; }

    }

    public partial class EmployeeType
    {
        public int EmployeeTypeId { get; set; }
        public string EmployeeTypeName { get; set; }
    }

    public partial class EmployeeTypeViewModel
    {
        public string statusCode { get; set; }
        public string success { get; set; }
        public List<EmployeeType> data { get; set; }
    }
}
