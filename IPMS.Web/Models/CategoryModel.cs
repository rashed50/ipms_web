﻿using System.Collections.Generic;

namespace IPMS.Web.Models
{
    public class CategoryModel
    {
        public bool CategoryIsActive { get; set; }
        public string CategoryName { get; set; }
        public int CategoryId { get; set; }
    }

    public partial class CategoryViewModel
    {
        public string statusCode { get; set; }
        public string success { get; set; }
        public List<CategoryModel> data { get; set; }
    }

    public class BrandModel
    {
        public int BrandId { get; set; }
        public string BrandName { get; set; }
        public bool BrandIsActive { get; set; }
        public int CategoryId { get; set; }
    }

    public partial class BrandViewModel
    {
        public string statusCode { get; set; }
        public string success { get; set; }
        public List<BrandModel> data { get; set; }
    }

    public class SizeModel
    {
        public int SizeId { get; set; }
        public string SizeName { get; set; }
        public bool SizeIsActive { get; set; }
        public int BrandId { get; set; }
    }

    public partial class SizeViewModel
    {
        public string statusCode { get; set; }
        public string success { get; set; }
        public List<SizeModel> data { get; set; }
    }

    public class WeightModel
    {
        public int WeightId { get; set; }
        public string WeightName { get; set; }
        public bool WeightIsActive { get; set; }
        public int SizeId { get; set; }
    }

    public partial class WeightViewModel
    {
        public string statusCode { get; set; }
        public string success { get; set; }
        public List<WeightModel> data { get; set; }
    }
}
