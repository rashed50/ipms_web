﻿using System.Collections.Generic;

namespace IPMS.Web.Models
{
    public class StockModel
    {
        public int StockId { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int BrandId { get; set; }
        public string BrandName { get; set; }
        public int SizeId { get; set; }
        public string SizeName { get; set; }
        public int WeightId { get; set; }
        public string WeightName { get; set; }
        public double LastSaleRate { get; set; }
        public double StockQuantity { get; set; }
    }

    public partial class StockViewModel
    {
        public string statusCode { get; set; }
        public string success { get; set; }
        public List<StockModel> data { get; set; }
    }
}
