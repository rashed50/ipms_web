﻿using System.Net.Http;
using IPMS.Web.Models.Config;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace IPMS.Web.Controllers
{
    public class BaseController : Controller
    {
        protected readonly ApiConfig _config;
        protected string apiBaseUrl;
        protected static HttpClient _httpClient;
        private readonly IHttpClientFactory _clientFactory;

        public BaseController(IOptions<ApiConfig> configAccessor, IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
            _config = configAccessor.Value;
            apiBaseUrl = _config.ApiBaseUrl;
            _httpClient = _clientFactory.CreateClient("forall");
            //Initialize(apiBaseUrl);
        }

        //private static void Initialize(string apiBaseUrl)
        //{
        //    _httpClient = new HttpClient();
        //    _httpClient.BaseAddress = new Uri(apiBaseUrl);
        //    _httpClient.DefaultRequestHeaders.Accept.Clear();
        //    _httpClient.DefaultRequestHeaders.Accept.Add(
        //        new MediaTypeWithQualityHeaderValue("application/json"));
        //}
    }
}
