﻿using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;
using IPMS.Web.Models;
using IPMS.Web.Models.Config;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace IPMS.Web.Controllers
{
    [AllowAnonymous]
    public class SizeController : BaseController
    {
        public SizeController(IOptions<ApiConfig> configAccessor,
            IHttpClientFactory clientFactory)
            : base(configAccessor, clientFactory)
        {
        }
        [HttpGet("/size/list")]
        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> GetSizes()
        {
            var response = await _httpClient.GetAsync("size/");
            response.EnsureSuccessStatusCode();
            string responseBody = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<SizeViewModel>(responseBody);
            var categories = result.data;

            var obj = new
            {
                recordsTotal = 0,
                recordsFiltered = 10,
                data = (from item in categories
                        select new string[]
                        {
                            item.SizeName,
                            item.SizeIsActive.ToString(),
                            item.SizeId.ToString()
                        }).ToArray()
            };

            return Json(obj);
        }

        [HttpGet]
        [Route("/size/add")]
        public IActionResult Add()
        {
            var model = new SizeModel();

            return View(model);
        }

        [HttpPost]
        //[Route("/brand/add")]
        public async Task<IActionResult> Add(SizeModel model)
        {
            try
            {
                var json = JsonConvert.SerializeObject(model);
                var data = new StringContent(json, Encoding.UTF8, "application/json");

                var response = await _httpClient.PostAsJsonAsync(
                    "size", model);
                response.EnsureSuccessStatusCode();

                string responseBody = await response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<SizeViewModel>(responseBody);

                if (result.success == "true")
                    return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View();
            }

            return View();
        }
    }
}
