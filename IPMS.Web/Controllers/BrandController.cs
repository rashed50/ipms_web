﻿using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;
using IPMS.Web.Models;
using IPMS.Web.Models.Config;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace IPMS.Web.Controllers
{
    [AllowAnonymous]
    public class BrandController : BaseController
    {
        public BrandController(IOptions<ApiConfig> configAccessor,
            IHttpClientFactory clientFactory)
            : base(configAccessor, clientFactory)
        {
        }
        [HttpGet("/brand/list")]
        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> GetBrands()
        {
            var response = await _httpClient.GetAsync("brand/");
            response.EnsureSuccessStatusCode();
            string responseBody = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<BrandViewModel>(responseBody);
            var categories = result.data;

            var obj = new
            {
                recordsTotal = 0,
                recordsFiltered = 10,
                data = (from item in categories
                        select new string[]
                        {
                            item.BrandName,
                            item.BrandIsActive.ToString(),
                            item.BrandId.ToString()
                        }).ToArray()
            };

            return Json(obj);
        }

        [HttpGet]
        [Route("/brand/add")]
        public IActionResult Add()
        {
            var model = new BrandModel();

            return View(model);
        }

        [HttpPost]
        //[Route("/brand/add")]
        public async Task<IActionResult> Add(BrandModel model)
        {
            try
            {
                var json = JsonConvert.SerializeObject(model);
                var data = new StringContent(json, Encoding.UTF8, "application/json");

                var response = await _httpClient.PostAsJsonAsync(
                    "brand", model);
                response.EnsureSuccessStatusCode();

                string responseBody = await response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<BrandViewModel>(responseBody);

                if (result.success == "true")
                    return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View();
            }

            return View();
        }
    }
}
