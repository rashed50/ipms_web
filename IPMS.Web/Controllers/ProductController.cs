﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using IPMS.Web.Models;
using IPMS.Web.Models.Config;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace IPMS.Web.Controllers
{
    [AllowAnonymous]
    public class ProductController : BaseController
    {
        public ProductController(IOptions<ApiConfig> configAccessor,
            IHttpClientFactory clientFactory)
            : base(configAccessor, clientFactory)
        {
        }

        [HttpGet("/product/list")]
        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> GetProductList()
        {
            var employeeList = new List<ProductModel>();
            var response = await _httpClient.GetAsync("productlist/");
            response.EnsureSuccessStatusCode();
            string responseBody = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<ProductViewModel>(responseBody);
            var productlist = result.data;

            var obj = new
            {
                recordsTotal = 0,
                recordsFiltered = 10,
                data = (from item in productlist
                        select new string[]
                        {
                            item.CategoryName,
                            item.BrandName,
                            item.SizeName,
                            item.WeightName,
                            item.tempPRate.ToString(),
                            item.IsProductActive.ToString(),
                            item.ProductListid.ToString()
                        }).ToArray()
            };

            return Json(obj);
        }
    }
}
