﻿using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;
using IPMS.Web.Models;
using IPMS.Web.Models.Config;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace IPMS.Web.Controllers
{
    [AllowAnonymous]
    public class WeightController : BaseController
    {
        public WeightController(IOptions<ApiConfig> configAccessor,
           IHttpClientFactory clientFactory)
           : base(configAccessor, clientFactory)
        {
        }
        [HttpGet("/weight/list")]
        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> GetWeights()
        {
            var response = await _httpClient.GetAsync("weight/");
            response.EnsureSuccessStatusCode();
            string responseBody = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<WeightViewModel>(responseBody);
            var categories = result.data;

            var obj = new
            {
                recordsTotal = 0,
                recordsFiltered = 10,
                data = (from item in categories
                        select new string[]
                        {
                            item.WeightName,
                            item.WeightIsActive.ToString(),
                            item.WeightId.ToString()
                        }).ToArray()
            };

            return Json(obj);
        }

        [HttpGet]
        [Route("/weight/add")]
        public IActionResult Add()
        {
            var model = new WeightModel();

            return View(model);
        }

        [HttpPost]
        //[Route("/weight/add")]
        public async Task<IActionResult> Add(WeightModel model)
        {
            try
            {
                var json = JsonConvert.SerializeObject(model);
                var data = new StringContent(json, Encoding.UTF8, "application/json");

                var response = await _httpClient.PostAsJsonAsync(
                    "weight", model);
                response.EnsureSuccessStatusCode();

                string responseBody = await response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<WeightViewModel>(responseBody);

                if (result.success == "true")
                    return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View();
            }

            return View();
        }
    }
}
