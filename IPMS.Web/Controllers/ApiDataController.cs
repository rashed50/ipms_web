﻿using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using IPMS.Web.Models;
using IPMS.Web.Models.Config;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace IPMS.Web.Controllers
{
    [AllowAnonymous]
    public class ApiDataController : BaseController
    {
        public ApiDataController(IOptions<ApiConfig> configAccessor,
            IHttpClientFactory clientFactory)
            : base(configAccessor, clientFactory)
        {
        }


        [HttpGet]
        public async Task<IActionResult> GetCategories()
        {
            try
            {
                var response = await _httpClient.GetAsync("category");
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<CategoryViewModel>(responseBody);

                var data = result.data;

                var obj = data.Select(x => new { id = x.CategoryId, name = x.CategoryName })
                    .ToList();

                return Ok(obj);
            }
            catch
            {
                return BadRequest();
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetBrands(int categoryId)
        {
            try
            {
                var response = await _httpClient.GetAsync($"brand/?categoryId={categoryId}");
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<BrandViewModel>(responseBody);

                var data = result.data;

                var obj = data.Select(x => new { id = x.BrandId, name = x.BrandName })
                    .ToList();

                return Ok(obj);
            }
            catch
            {
                return BadRequest();
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetSizes(int brandId)
        {
            try
            {
                var response = await _httpClient.GetAsync($"size/?brandId={brandId}");
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<SizeViewModel>(responseBody);

                var data = result.data;

                var obj = data.Select(x => new { id = x.SizeId, name = x.SizeName })
                    .ToList();

                return Ok(obj);
            }
            catch
            {
                return BadRequest();
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetWeights(int sizeId)
        {
            try
            {
                var response = await _httpClient.GetAsync($"weight/?sizeId={sizeId}");
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<WeightViewModel>(responseBody);

                var data = result.data;

                var obj = data.Select(x => new { id = x.WeightId, name = x.WeightName })
                    .ToList();

                return Ok(obj);
            }
            catch
            {
                return BadRequest();
            }
        }
    }
}
