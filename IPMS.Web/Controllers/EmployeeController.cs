﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;
using IPMS.Web.Controllers;
using IPMS.Web.Models;
using IPMS.Web.Models.Config;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace AdminLTE.MVC.Controllers
{
    [AllowAnonymous]
    public class EmployeeController : BaseController
    {
        public EmployeeController(IOptions<ApiConfig> configAccessor,
            IHttpClientFactory clientFactory)
            : base(configAccessor, clientFactory)
        {
        }

        [HttpGet("/employee/list")]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [Route("/employee/add")]
        public IActionResult Add()
        {
            var model = new EmployeeModel();

            return View(model);
        }

        [HttpPost]
        //[Route("/employee/add")]
        public async Task<IActionResult> Add(EmployeeModel model)
        {
            try
            {
                model.DesignationId = 1;
                model.FactoryId = 1;
                model.FactoryUnitId = 1;
                model.SalaryGradeId = 0;
                model.Photo = "photo";

                var json = JsonConvert.SerializeObject(model);
                var data = new StringContent(json, Encoding.UTF8, "application/json");

                var response = await _httpClient.PostAsJsonAsync(
                    "employeeinformation", model);
                response.EnsureSuccessStatusCode();

                string responseBody = await response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<EmployeeViewModel>(responseBody);

                if (result.success == "true")
                    return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View();
            }

            return View();
        }

        [HttpGet]
        public async Task<IActionResult> GetEmployeeType(int divisionId)
        {
            //Console.WriteLine(connection.State);
            var districtList = new List<EmployeeType>();
            try
            {
                var response = await _httpClient.GetAsync("employeetype");
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<EmployeeTypeViewModel>(responseBody);
                districtList = result.data;


                var obj = districtList.Select(x => new { id = x.EmployeeTypeId, name = x.EmployeeTypeName })
                    .ToList();

                return Ok(obj);
            }
            catch
            {
                return BadRequest();
            }


        }

        [HttpGet]
        public async Task<IActionResult> GetDistricts(int divisionId)
        {
            //Console.WriteLine(connection.State);
            var districtList = new List<District>();
            try
            {
                var response = await _httpClient.GetAsync("districts");
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<DistrictViewModel>(responseBody);
                districtList = result.data;


                var obj = districtList.Where(x => x.DivisionId == divisionId)
                    .Select(x => new { id = x.DistrictId, name = x.DistrictName })
                    .ToList();

                return Ok(obj);
            }
            catch
            {
                return BadRequest();
            }


        }

        [HttpGet]
        public async Task<IActionResult> GetDivisions(int countryId)
        {
            try
            {
                var response = await _httpClient.GetAsync("divisions");
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<DivisionViewModel>(responseBody);

                var data = result.data;

                var obj = data.Where(x => x.CountryId == countryId)
                    .Select(x => new { id = x.DivisionId, name = x.DivisionName })
                    .ToList();

                return Ok(obj);
            }
            catch
            {
                return BadRequest();
            }


        }

        [HttpGet]
        public async Task<IActionResult> GetCountry()
        {
            try
            {
                var response = await _httpClient.GetAsync("country");
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<CountryViewModel>(responseBody);

                var data = result.data;

                var obj = data.Select(x => new { id = x.CountryId, name = x.CountryName })
                    .ToList();

                return Ok(obj);
            }
            catch
            {
                return BadRequest();
            }


        }

        [HttpGet]
        public async Task<IActionResult> GetUpazila(int districtId)
        {
            var employeeList = new List<Upazila>();
            try
            {
                var response = await _httpClient.GetAsync("upazila");
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<UpazilaViewModel>(responseBody);
                employeeList = result.data;

                var obj = employeeList.Where(x => x.DistrictId == districtId)
                     .Select(x => new { id = x.UpazilaId, name = x.UpazilaName })
                     .ToList();

                return Ok(obj);
            }
            catch
            {
                return BadRequest();
            }


        }

        public async Task<IActionResult> GetEmployees()
        {
            var employeeList = new List<EmployeeModel>();
            var response = await _httpClient.GetAsync("employeeinformation");
            response.EnsureSuccessStatusCode();
            string responseBody = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<EmployeeViewModel>(responseBody);
            employeeList = result.data;

            var obj = new
            {
                recordsTotal = 0,
                recordsFiltered = 10,
                data = (from item in employeeList
                        select new string[]
                        {
                            item.EmployeeName,
                            item.EmpMobileNo,
                            item.NID,
                            item.Photo
                        }).ToArray()
            };

            return Json(obj);
        }
    }
}
