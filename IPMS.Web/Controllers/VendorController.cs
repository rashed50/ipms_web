﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;
using IPMS.Web.Models.Config;
using IPMS.Web.Models.Vendors;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace IPMS.Web.Controllers
{
    [AllowAnonymous]
    public class VendorController : BaseController
    {
        public VendorController(IOptions<ApiConfig> configAccessor,
               IHttpClientFactory clientFactory)
               : base(configAccessor, clientFactory)
        {
        }
        [HttpGet("/vendor/list")]
        // GET: VendorController
        public ActionResult Index()
        {
            return View();
        }

        // GET: VendorController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        [HttpGet]
        [Route("/vendor/add")]
        // GET: VendorController/Create
        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        // GET: VendorController/Create
        public async Task<ActionResult> Add(Vendor model)
        {
            try
            {
                model.VendorAccountId = 1;
                model.OpeningDate = DateTime.Now;
                model.VendorPhoto = "photo";

                var json = JsonConvert.SerializeObject(model);
                var data = new StringContent(json, Encoding.UTF8, "application/json");

                var response = await _httpClient.PostAsJsonAsync(
                    "vendor", model);
                response.EnsureSuccessStatusCode();

                string responseBody = await response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<VendorViewModel>(responseBody);

                if (result.success == "true")
                    return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View();
            }

            return View();
        }

        // POST: VendorController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: VendorController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: VendorController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: VendorController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: VendorController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }


        public async Task<IActionResult> GetVendors()
        {
            var employeeList = new List<Vendor>();
            var response = await _httpClient.GetAsync("vendor");
            response.EnsureSuccessStatusCode();
            string responseBody = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<VendorViewModel>(responseBody);
            employeeList = result.data;

            var obj = new
            {
                recordsTotal = 0,
                recordsFiltered = 10,
                data = (from item in employeeList
                        select new string[]
                        {
                            item.VendorName,
                            item.ContactNumber,
                            item.Address,
                            item.VendorPhoto
                        }).ToArray()
            };

            return Json(obj);
        }
    }
}
