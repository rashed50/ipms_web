﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace IPMSTestConsole
{
    class Program
    {
        static HttpClient client = new HttpClient();

        //static void ShowProduct(EmployeeModel product)
        //{
        //    Console.WriteLine($"Name: {product.Name}\tPrice: " +
        //        $"{product.Price}\tCategory: {product.Category}");
        //}

        static async Task<Uri> CreateProductAsync(EmployeeModel product)
        {
            try
            {

                HttpResponseMessage response = await client.PostAsJsonAsync(
                    "api/employeeinformation", product);
                response.EnsureSuccessStatusCode();

                // return URI of the created resource.
                return response.Headers.Location;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return null; 
        }

        //static async Task<EmployeeModel> GetProductAsync(string path)
        //{
        //    EmployeeModel product = null;
        //    HttpResponseMessage response = await client.GetAsync(path);
        //    if (response.IsSuccessStatusCode)
        //    {
        //        product = await response.Content.ReadAsAsync<EmployeeModel>();
        //    }
        //    return product;
        //}

        //static async Task<EmployeeModel> UpdateProductAsync(EmployeeModel product)
        //{
        //    HttpResponseMessage response = await client.PutAsJsonAsync(
        //        $"api/products/{product.Id}", product);
        //    response.EnsureSuccessStatusCode();

        //    // Deserialize the updated product from the response body.
        //    product = await response.Content.ReadAsAsync<Product>();
        //    return product;
        //}

        //static async Task<HttpStatusCode> DeleteProductAsync(string id)
        //{
        //    HttpResponseMessage response = await client.DeleteAsync(
        //        $"api/products/{id}");
        //    return response.StatusCode;
        //}

        static void Main()
        {
            RunAsync().GetAwaiter().GetResult();
        }

        static async Task RunAsync()
        {
            // Update port # in the following line.
            client.BaseAddress = new Uri("http://localhost:5000/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            try
            {
                // Create a new product
                EmployeeModel model = new EmployeeModel
                {
                    EmployeeName = "Gizmo",
                    EmpMobileNo = "100",
                    FatherName = "Widgets",
                    JoinDate = DateTime.Now,
                    NID = "00001",
                    PresentSalary = 10000,
                    StartingSalary = 10000,
                    ContactNumber = "100",
                    ReviewDate = DateTime.Now
                };


                model.CountryId = 1;
                model.DesignationId = 1;
                model.DistrictId = 1;
                model.DivisionId = 1;
                model.FactoryId = 1;
                model.FactoryUnitId = 1;
                model.SalaryGradeId = 0;
                model.UpazilaId = 1;
                model.Photo = "photo";
                model.EmployeeTypeId = 1;

                var url = await CreateProductAsync(model);
                Console.WriteLine($"Created at {url}");

                // Get the product
                //product = await GetProductAsync(url.PathAndQuery);
                //ShowProduct(product);

                // Update the product
                //Console.WriteLine("Updating price...");
                //product.Price = 80;
                //await UpdateProductAsync(product);

                // Get the updated product
                //product = await GetProductAsync(url.PathAndQuery);
                //ShowProduct(product);

                // Delete the product
                //var statusCode = await DeleteProductAsync(product.Id);
                //Console.WriteLine($"Deleted (HTTP Status = {(int)statusCode})");

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.ReadLine();
        }
    }

    public class EmployeeModel
    {
        public string EmployeeName { get; set; }
        public string ContactNumber { get; set; }
        public string FatherName { get; set; }
        public string EmpMobileNo { get; set; }
        public string NID { get; set; }
        public string Photo { get; set; }
        public DateTime JoinDate { get; set; }
        public DateTime ReviewDate { get; set; }
        public int CountryId { get; set; }
        public int DivisionId { get; set; }
        public int DistrictId { get; set; }
        public int UpazilaId { get; set; }
        public int FactoryId { get; set; }
        public int FactoryUnitId { get; set; }
        public int DesignationId { get; set; }
        public int EmployeeTypeId { get; set; }
        public int SalaryGradeId { get; set; }
        public int PresentSalary { get; set; }
        public int StartingSalary { get; set; }

        //public List<object> GetAllEmployee()
        //      {

        //      }
    }

}